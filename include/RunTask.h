/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef RUNTASK_H
#define RUNTASK_H

#include "Potentiometer.h"
#include "SmartExpGlobal.h"
#include "Task.h"
#include "Temperature.h"
#include "UltrasonicSensor.h"

class RunTask : public Task
{
    UltrasonicSensor* sonar;
    unsigned pinEcho;
    unsigned pinTrigger;
    Potentiometer* potentiometer;
    unsigned potentiometerPin;
    unsigned potentiometerMinHz;
    unsigned potentiometerMaxHz;
    Temperature* temp;
    unsigned tempPin;

  public:
    RunTask(unsigned pinEcho,
            unsigned pinTrigger,
            unsigned potentiometerPin,
            unsigned potentiometerMinHz,
            unsigned potentiometerMaxHz,
            unsigned tempPin);
    void init(int period);
    void tick() override;
};

#endif