/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef LEDIMPL_H
#define LEDIMPL_H

#include "Led.h"
#include <Arduino.h>

class LedImpl : public Led
{
    unsigned pin;
    bool isOn;

  public:
    explicit LedImpl(unsigned pin);

    void switchOn() override;
    void switchOff() override;
    bool isTurnedOn() override;
};

#endif