/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef BUTTON_H
#define BUTTON_H

class Button
{
  public:
    virtual bool isPressed() = 0;
};

#endif