/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef MOTIONSENSORIMPL_H
#define MOTIONSENSORIMPL_H

#include "MotionSensor.h"
#include <Arduino.h>

class MotionSensorImpl : public MotionSensor
{
  private:
    unsigned pin;
    bool isCalibrated;
    const unsigned calibrationDuration;

  public:
    explicit MotionSensorImpl(unsigned pin);
    bool read();
};

#endif