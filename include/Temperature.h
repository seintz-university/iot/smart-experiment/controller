/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef TEMPERATURE_H
#define TEMPERATURE_H

class Temperature
{
  public:
    virtual unsigned read() = 0;
};

#endif