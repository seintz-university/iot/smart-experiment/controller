/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef TEMPERATUREIMPL_H
#define TEMPERATUREIMPL_H

#include "Temperature.h"
#include <Arduino.h>

class TemperatureImpl : public Temperature
{
    unsigned pin;

  public:
    TemperatureImpl(unsigned pin);
    unsigned read() override;
};

#endif