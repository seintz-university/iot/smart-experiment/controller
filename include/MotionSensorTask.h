/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef MOTIONSENSORTASK_H
#define MOTIONSENSORTASK_H

#include "MotionSensor.h"
#include "SmartExpGlobal.h"
#include "Task.h"

class MotionSensorTask : public Task
{
    MotionSensor* motionSensor;
    int pin;

  public:
    explicit MotionSensorTask(unsigned pin);
    void init(int period);
    void tick() override;
};

#endif