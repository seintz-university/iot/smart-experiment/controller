/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "SerialIOTask.h"
#include "SmartExpGlobal.h"

SerialIOTask::SerialIOTask() {}

void
SerialIOTask::init(int period)
{
    Task::init(period);
    msgService = new MsgServiceClass();
    msgService->init();
}

// https://arduino.stackexchange.com/questions/1013/how-do-i-split-an-incoming-string
String
getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i + 1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void
SerialIOTask::tick()
{
    switch (SmartExpGlobal::mode) {
        case Modes::WELCOME:
            MsgService.sendMsg("Welcome to Smart Experiment");
            break;
        case Modes::SLEEP:
            MsgService.sendMsg("System sleeping, approach to wake up");
            break;
        case Modes::STARTEXP:
            MsgService.sendMsg("Experiment Started");
            break;
        case Modes::RUN:
            MsgService.sendMsg("Calibrating Sensor");
            break;
        case Modes::SCAN:
            MsgService.sendMsg("Experiment Running | " +
                               (String)SmartExpGlobal::d_graph + " | " +
                               (String)SmartExpGlobal::t_graph + " | " +
                               (String)SmartExpGlobal::v_graph + " | " +
                               (String)SmartExpGlobal::a_graph);
            break;
        case Modes::STOPEXP:
            MsgService.sendMsg("Experiment Ended");
            break;
        case Modes::END:
            MsgService.sendMsg("Press Button to start over");
            if (MsgService.isMsgAvailable()) {
                Msg* message = MsgService.receiveMsg();
                String msgContent = message->getContent();
                if (msgContent == "pressEnd") {
                    SmartExpGlobal::gotInput = true;
                }
                delete message;
            }
            break;
    }
}

/**
 * // SERIAL INPUT
    if (MsgService.isMsgAvailable()) {
        Msg* message = MsgService.receiveMsg();
        String msgContent = message->getContent();

        if (msgContent.indexOf(SmartExpGlobal::consoleCommandString) >= 0) {
            if (msgContent.indexOf(SmartExpGlobal::movementCommandString) >=
                0) {
                if (msgContent.indexOf(SmartExpGlobal::movementLeftString) >=
                    0) {
                    SmartExpGlobal::isMoving = true;
                    SmartExpGlobal::directionRight = false;
                } else if (msgContent.indexOf(
                             SmartExpGlobal::movementRightString) >= 0) {
                    SmartExpGlobal::isMoving = true;
                    SmartExpGlobal::directionRight = true;
                }
            }

            if (msgContent.indexOf(
                  SmartExpGlobal::scanDurationCommandString) >= 0) {
                SmartExpGlobal::scanDuration =
                  getValue(msgContent, ':', 5).toInt();
                SmartExpGlobal::serialOutputMessages.add(
                  (String)SmartExpGlobal::scanDurationCommandString +
                  SmartExpGlobal::scanDuration);
                SmartExpGlobal::sliceDelay = SmartExpGlobal::scanDuration *
                                               1000 /
                                               (SmartExpGlobal::nSlices * 2);
            }

            delete message;
        }
    }

    // SERIAL OUTPUT
    if (SmartExpGlobal::serialOutputMessages.size() > 0) {
        Msg* message;
        for (int i = 0; i < SmartExpGlobal::serialOutputMessages.size();
             i++) {
            message = new Msg(SmartExpGlobal::serialOutputMessages.get(i));
            MsgService.sendMsg((String)millis() + ":" +
                               SmartExpGlobal::controllerCommandString +
                               message->getContent());
            SmartExpGlobal::serialOutputMessages.remove(i);
        }
        delete message;
    }
    **/