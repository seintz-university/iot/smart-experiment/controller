/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "RunTask.h"
#include "Arduino.h"
#include "PotentiometerImpl.h"
#include "SmartExpGlobal.h"
#include "TemperatureImpl.h"
#include "UltrasonicSensorImpl.h"

RunTask::RunTask(unsigned pinEcho,
                 unsigned pinTrigger,
                 unsigned potentiometerPin,
                 unsigned potentiometerMinHz,
                 unsigned potentiometerMaxHz,
                 unsigned tempPin)
{
    this->pinEcho = pinEcho;
    this->pinTrigger = pinTrigger;
    this->potentiometerPin = potentiometerPin;
    this->potentiometerMinHz = potentiometerMinHz;
    this->potentiometerMaxHz = potentiometerMaxHz;
    this->tempPin = tempPin;
}

void
RunTask::init(int period)
{
    Task::init(period);
    sonar = new UltrasonicSensorImpl(this->pinTrigger, this->pinEcho);
    potentiometer = new PotentiometerImpl(this->potentiometerPin,
                                          this->potentiometerMinHz,
                                          this->potentiometerMaxHz);
    temp = new TemperatureImpl(this->tempPin);
}

void
RunTask::tick()
{
    if (SmartExpGlobal::mode == Modes::STARTEXP) {
        SmartExpGlobal::scanFrequency = potentiometer->read();
        SmartExpGlobal::temp = temp->read();
        SmartExpGlobal::distance = sonar->read();

        if (SmartExpGlobal::distance != 0) {
            SmartExpGlobal::led_green->switchOn();
            SmartExpGlobal::led_red->switchOff();
            SmartExpGlobal::mode = Modes::SCAN;
        } else {
            SmartExpGlobal::led_green->switchOn();
            delay(SmartExpGlobal::errorTime / 2);
            SmartExpGlobal::led_green->switchOff();
            delay(SmartExpGlobal::errorTime / 2);
            SmartExpGlobal::mode = Modes::WELCOME;
        }
    }
}