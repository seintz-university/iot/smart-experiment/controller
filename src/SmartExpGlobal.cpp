/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#define PIN_LED_RED 3
#define PIN_LED_GREEN 4

#define ERROR_TIME 2
#define MAX_TIME 20
#define SLEEP_TIME 5
#define MINFREQ 1
#define MAXFREQ 50
#define MAXVEL 100

#include "SmartExpGlobal.h"

Led* SmartExpGlobal::led_green = new LedImpl(PIN_LED_GREEN);
Led* SmartExpGlobal::led_red = new LedImpl(PIN_LED_RED);
Modes SmartExpGlobal::mode = Modes::WELCOME;

unsigned SmartExpGlobal::errorTime = ERROR_TIME;
unsigned SmartExpGlobal::sleepTime = SLEEP_TIME;
unsigned SmartExpGlobal::maxTime = MAX_TIME;
unsigned SmartExpGlobal::minFreq = MINFREQ;
unsigned SmartExpGlobal::maxFreq = MAXFREQ;
unsigned SmartExpGlobal::maxSpeed = MAXVEL;
float SmartExpGlobal::temp = 0.0;
float SmartExpGlobal::d_old = 0.0;
float SmartExpGlobal::t_old = 0.0;

float SmartExpGlobal::d_graph = 0.0;
float SmartExpGlobal::t_graph = 0.0;
float SmartExpGlobal::v_graph = 0.0;
float SmartExpGlobal::a_graph = 0.0;

bool SmartExpGlobal::gotInput = false;



// welcome
bool SmartExpGlobal::shouldGoToSleep = false;
bool SmartExpGlobal::errorState = false;
bool SmartExpGlobal::beginNew = true;

// sleep
bool SmartExpGlobal::allowedToDetect = false;
bool SmartExpGlobal::objectDetected = false;

// run
bool SmartExpGlobal::startExp = false;
bool SmartExpGlobal::startScan = false;
bool SmartExpGlobal::expTimeOver = true;

// scan
bool SmartExpGlobal::endExp = true;
double SmartExpGlobal::distance = 0;
uint8_t SmartExpGlobal::scanFrequency = 1;

// dunno
bool SmartExpGlobal::welcomeState = true;


// old
bool SmartExpGlobal::shouldScan = false;
int SmartExpGlobal::nSlices = 16;
bool SmartExpGlobal::isMoving = false;
bool SmartExpGlobal::directionRight = false;
bool SmartExpGlobal::isAlarmState = false;
bool SmartExpGlobal::isTrackingState = false;
const int SmartExpGlobal::scanDurationMin = 2;
const int SmartExpGlobal::scanDurationMax = 10;
bool SmartExpGlobal::modeChanged = false;
unsigned SmartExpGlobal::sliceDelay = 0;
float SmartExpGlobal::currentAngle = 0;
int SmartExpGlobal::scanDuration = SmartExpGlobal::scanDurationMin;

// IO string
String SmartExpGlobal::detectingString = "DETECTING";
String SmartExpGlobal::consoleCommandString = "CONSOLE:";
String SmartExpGlobal::controllerCommandString = "CONTROLLER:";
String SmartExpGlobal::movementCommandString = "MOVE:";
String SmartExpGlobal::scanDurationCommandString = "DURATION:";
String SmartExpGlobal::modeCommandString = "MODE:";
String SmartExpGlobal::movementResponseString = "MOVING:";
String SmartExpGlobal::detectionString = "DETECTED:";
String SmartExpGlobal::alarmString = "ALARM";
String SmartExpGlobal::trackingString = "TRACKING";
String SmartExpGlobal::movementLeftString = "LEFT";
String SmartExpGlobal::movementRightString = "RIGHT";
String SmartExpGlobal::modeAutoString = "AUTO";
String SmartExpGlobal::modeSingleString = "SINGLE";
String SmartExpGlobal::modeManualString = "MANUAL";


LinkedList<String> SmartExpGlobal::serialOutputMessages = LinkedList<String>();
