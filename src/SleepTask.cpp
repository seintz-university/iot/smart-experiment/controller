/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "SleepTask.h"
#include <avr/power.h>
#include <avr/sleep.h>

unsigned int SleepTask::pin = 0;

SleepTask::SleepTask(unsigned int pin)
{
    SleepTask::pin = pin;
}

void
SleepTask::init(int period)
{
    Task::init(period);
}

void
SleepTask::tick()
{
    if (SmartExpGlobal::mode == Modes::SLEEP) {
        Serial.println("sleeping");
        SmartExpGlobal::led_red->switchOff();
        SmartExpGlobal::led_green->switchOff();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
        attachInterrupt(
          digitalPinToInterrupt(SleepTask::pin), SleepTask::wakeUp, RISING);
        sleep_mode();
    }
}

void
SleepTask::wakeUp()
{
    Serial.println("waking up");
    sleep_disable();
    detachInterrupt(SleepTask::pin);
    SmartExpGlobal::mode = Modes::WELCOME;
}
