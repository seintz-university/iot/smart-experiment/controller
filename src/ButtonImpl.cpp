/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "ButtonImpl.h"

ButtonImpl::ButtonImpl(unsigned pin)
{
    this->pin = pin;
    pinMode(pin, INPUT);
}

bool
ButtonImpl::isPressed()
{
    return digitalRead(pin) == HIGH;
}