/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "ServoMotorImpl.h"

ServoMotorImpl::ServoMotorImpl(unsigned pin)
{
    this->pin = pin;
    this->angle = 0;
}

unsigned
ServoMotorImpl::getPosition()
{
    return this->angle;
}

void
ServoMotorImpl::moveLeft(unsigned angle)
{
    int targetAngle = this->getPosition();
    targetAngle -= angle;
    if (targetAngle < 0) {
        targetAngle = 0;
    }
    this->setPosition(targetAngle);
}

void
ServoMotorImpl::moveRight(unsigned angle)
{
    unsigned targetAngle = this->getPosition();
    targetAngle += angle;
    if (targetAngle > 180) {
        targetAngle = 180;
    }
    this->setPosition(targetAngle);
}

void
ServoMotorImpl::setPosition(unsigned angle)
{
    motor.attach(this->pin);
    this->angle = angle;
    motor.write(750 + this->angle * angleCoefficient);
}
