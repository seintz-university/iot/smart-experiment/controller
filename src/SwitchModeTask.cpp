/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "SwitchModeTask.h"
#include "ButtonImpl.h"
#include "SmartExpGlobal.h"

SwitchModeTask::SwitchModeTask(unsigned pinB1, unsigned pinB2)
{
    b1 = new ButtonImpl(pinB1);
    b2 = new ButtonImpl(pinB2);
}

void
SwitchModeTask::init(int period)
{
    Task::init(period);
}

void
SwitchModeTask::tick()
{
    Modes newMode = SmartExpGlobal::mode;
    if (b1->isPressed()) {
        newMode = Modes::STARTEXP;
    } else if (b2->isPressed()) {
        newMode = Modes::STOPEXP;
    }
    SmartExpGlobal::mode = newMode;
}
